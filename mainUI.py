# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainUI_origin.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.
import os, sys

from PyQt5 import QtCore, QtWidgets
import QRcode
import MessageBox
from Resources import rgbcolor_index

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(760, 553)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setAutoFillBackground(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setAutoFillBackground(True)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.lineEdit_InputQRAddress = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_InputQRAddress.setMinimumSize(QtCore.QSize(250, 0))
        self.lineEdit_InputQRAddress.setAutoFillBackground(True)
        self.lineEdit_InputQRAddress.setObjectName("lineEdit_InputQRAddress")
        self.horizontalLayout.addWidget(self.lineEdit_InputQRAddress)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pushButtonGenerate = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonGenerate.setAutoFillBackground(True)
        self.pushButtonGenerate.setObjectName("pushButtonGenerate")
        self.horizontalLayout.addWidget(self.pushButtonGenerate)
        self.pushButtonClean = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonClean.setAutoFillBackground(True)
        self.pushButtonClean.setObjectName("pushButtonClean")
        self.horizontalLayout.addWidget(self.pushButtonClean)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem2 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem2)
        self.checkBox_icon = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_icon.setObjectName("checkBox")
        self.horizontalLayout_4.addWidget(self.checkBox_icon)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem3)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_4.addWidget(self.label_5)
        self.comboBox_centralcolor = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_centralcolor.setObjectName("comboBox_centralcolor")
        self.horizontalLayout_4.addWidget(self.comboBox_centralcolor)
        spacerItem4 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.comboBox_edgecolor = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_edgecolor.setObjectName("comboBox_edgecolor")
        self.horizontalLayout_4.addWidget(self.comboBox_edgecolor)
        spacerItem5 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem5)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        spacerItem6 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout_2.addItem(spacerItem6)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setAutoFillBackground(True)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem7)
        self.pushButtonFromImg = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonFromImg.setMinimumSize(QtCore.QSize(200, 0))
        self.pushButtonFromImg.setAutoFillBackground(True)
        self.pushButtonFromImg.setObjectName("pushButtonFromImg")
        self.horizontalLayout_2.addWidget(self.pushButtonFromImg)
        spacerItem8 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem8)
        self.pushButtonFromCamera = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonFromCamera.setMinimumSize(QtCore.QSize(200, 0))
        self.pushButtonFromCamera.setAutoFillBackground(True)
        self.pushButtonFromCamera.setObjectName("pushButtonFromCamera")
        self.horizontalLayout_2.addWidget(self.pushButtonFromCamera)
        spacerItem9 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem9)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setAutoFillBackground(True)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_2.addWidget(self.label_4)
        self.plainText_ReturnQRCode = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainText_ReturnQRCode.setAutoFillBackground(True)
        self.plainText_ReturnQRCode.setReadOnly(True)
        self.plainText_ReturnQRCode.setOverwriteMode(True)
        self.plainText_ReturnQRCode.setBackgroundVisible(False)
        self.plainText_ReturnQRCode.setObjectName("plainText_ReturnQRCode")
        self.verticalLayout_2.addWidget(self.plainText_ReturnQRCode)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 760, 24))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuAbout = QtWidgets.QMenu(self.menubar)
        self.menuAbout.setObjectName("menuAbout")

        MainWindow.setMenuBar(self.menubar)
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.menuFile.addAction(self.actionExit)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.actionAbout)

#按钮的信号关联
        self.retranslateUi(MainWindow)
        self.actionExit.triggered.connect(MainWindow.close)
        self.actionAbout.triggered.connect(lambda:MessageBox.About("关于","版本v0.2")) #菜单栏关于
        self.pushButtonClean.clicked.connect(self.lineEdit_InputQRAddress.clear) #清除输入
        self.pushButtonFromImg.clicked.connect(lambda:self.FromImg()) #从图片按钮
        self.pushButtonFromCamera.clicked.connect(lambda:self.FromCamera()) #从相机按钮
        self.pushButtonGenerate.clicked.connect(lambda:self.GenerateQR()) #生成按钮

# 下来菜单颜色选取
        self.comboBox_centralcolor.addItems(rgbcolor_index.name)
        self.comboBox_edgecolor.addItems(rgbcolor_index.name)
        #self.comboBox_centralcolor.currentTextChanged.connect(MainWindow.update)
        #self.comboBox_edgecolor.currentTextChanged.connect(MainWindow.update)


        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def GenerateQR(self): # 生成QR图片
        img = QRcode.QRCreate(self.lineEdit_InputQRAddress.text(),
                              self.checkBox_icon.isChecked(),
                              self.comboBox_centralcolor.currentIndex(),
                              self.comboBox_edgecolor.currentIndex())

        filePath, filetype = QtWidgets.QFileDialog.getSaveFileName(None,'存储位置','./',filter='PNG File (*.png)') #保存位置
        if filePath == '':
            pass
        else:
            img.save(filePath)


    def FromImg(self): #从图片读取QR码
            filePath, filetype = QtWidgets.QFileDialog.getOpenFileName(None,'选择图片','./','*.jpg *.gif *.png *.jpeg')
            if filePath == '':
                pass

            else:
                filePath = os.path.join(os.path.dirname(sys.argv[0]),filePath)
                text, _ = QRcode.QRImgCapture(filePath)
                #print(text)
                if text == '':
                    error_msg = MessageBox.Error("错误", "无法读取图片") #如果报错

                else:
                    self.plainText_ReturnQRCode.setPlainText(str(text)) #输出到字符框


    def FromCamera(self): #从摄像头读取QR码
        text = QRcode.QRCameraCapture()
        #print(text)
        self.plainText_ReturnQRCode.setPlainText(str(text)) #输出到字符框

# 功能键名称显示及翻译
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "二维码快速创建和读取软件v0.2"))
        self.label.setText(_translate("Generate QR Code", "二维码生成"))
        self.label_2.setText(_translate("QR Code info", "二维码信息："))
        self.pushButtonGenerate.setText(_translate("Generate", "生成"))
        self.pushButtonClean.setText(_translate("Clear", "清空"))
        self.checkBox_icon.setText(_translate("Icon checkbox", "是否添加图标"))
        self.label_5.setText(_translate("Central color", "中心颜色"))
        self.label_6.setText(_translate("Edge Color", "边缘颜色"))
        self.label_3.setText(_translate("Read QR Code", "二维码识别"))
        self.pushButtonFromImg.setText(_translate("Read from image", "从图片识别"))
        self.pushButtonFromCamera.setText(_translate("Read from camera", "从摄像头识别"))
        self.label_4.setText(_translate("QR code content", "二维码内容"))
        self.menuFile.setTitle(_translate("File", "文件"))
        self.menuAbout.setTitle(_translate("About", "关于"))
        self.actionAbout.setText(_translate("About", "关于"))
        self.actionExit.setText(_translate("Exit", "退出"))

