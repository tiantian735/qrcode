#
#
#
import qrcode as qrcode
from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.colormasks import SquareGradiantColorMask
import cv2 as cv
import os, sys
import MessageBox
from Resources import rgbcolor_index


# 创建QRcode
def QRCreate(url, iconstate, Center_color, Edge_color):
    qr = qrcode.QRCode(
        version=None,  # 二维码的格子矩阵大小
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=10,
        border=4,
    )

    qr.add_data(url)  # 向二维码添加数据
    qr.make(fit=True) #二维码大小（版本）自适应

#是否添加图标判定
    if iconstate == True:
        icon = 'Resources/icon.JPG'
    else:
        icon = None
        #os.path.join(os.path.dirname(sys.argv[0]),"Resources","icon.JPG")
    #print(icon)

    # 更改QR的背景和绘画颜色
    img = qr.make_image(image_factory=StyledPilImage,
                        color_mask=SquareGradiantColorMask(back_color=(255, 255, 255),
                                                           center_color=rgbcolor_index.index[Center_color],
                                                           edge_color=rgbcolor_index.index[Edge_color]),
                        embeded_image_path=icon)

    #img.show()
    return img


# 从图片识别QR码：
def QRImgCapture(src):
    if type(src) is str:
        src = cv.imread(src)  # 读取图片

    if src is None:
        codeinfo, points = '', ''

    else:
        gray = cv.cvtColor(src, cv.COLOR_BGR2GRAY)  # 图像灰阶预处理
        # cv.imshow("gray",gray)

        qrcoder = cv.QRCodeDetector()  # 调用cv2中QRCodeDetector class

        codeinfo, points, straight_qrcode = qrcoder.detectAndDecode(gray)  # 检测和解码
        #print(codeinfo)
        # print(points)
        #result = np.copy(src)

        # cv.drawContours(result, [np.int32(points)],0,(0,0,255),2)
        # print("qrcode: %s" % codeinfo)
        # print(straight_qrcode)

    return codeinfo, points


# 从摄像头识别QR码：
def QRCameraCapture():
    # 调用openCV图片采集
    cap = cv.VideoCapture(0)

    while True:
        try:
            ret, frame = cap.read()  # 读取帧
            cv.imshow('Press ESC to exit', frame)
            code, pts = QRImgCapture(frame)  # 分析和解码帧
            if code != '' or cv.waitKey(1) & 0xFF == 27:  # 识别到有二维码时,按esc键退出
            # if code != '' or cv.waitKey(1) & 0xFF == ord('q'):
                break
        except:
            MessageBox.Error("错误", "无法检测到摄像头")
            code = ''
            break

    cap.release()
    cv.destroyWindow('Press ESC to exit')
    cv.waitKey(1)
    return code
