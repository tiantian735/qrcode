# pyqt5下各种消息弹窗

from PyQt5.QtWidgets import QMessageBox


# 错误
def Error(title, message):
    msg_box = QMessageBox(QMessageBox.Critical, title, message)
    msg_box.exec_()

# 警告
def Warning(title, message):
    msg_box = QMessageBox(QMessageBox.Warning, title, message, QMessageBox.Cancel)
    msg_box.exec_()

# 关于
def About(title, message):
    msg_box = QMessageBox(QMessageBox.NoIcon, title, message)
    msg_box.exec_()